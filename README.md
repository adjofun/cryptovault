# CryptoVault
This library is intended to provide secure data storage for application data.
Currently this library delegates security management (keystore init, key generation/retrieval) to client class.
Refer to JavaDoc for more info, and to unit tests for examples.

# Building
Use Maven.
`mvn package` should produce this library's jar into project directory.