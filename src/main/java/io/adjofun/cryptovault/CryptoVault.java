package io.adjofun.cryptovault;

import io.adjofun.cryptovault.exception.CodecException;
import io.adjofun.cryptovault.exception.CryptoVaultException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.io.*;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * This class provides simple map-like interface to store values of any type with a codec in provided directory.
 * Values are encrypted with provided key and cipher, and stored in file with hashed name.
 * Note that security management must happen outside of this class.
 */
public class CryptoVault {
    private SecretKey secretKey;
    private Cipher cipher;
    private Path vaultPath;

    private CryptoVault(Builder builder) {
        this.secretKey = builder.secretKey;
        this.cipher = builder.cipher;
        this.vaultPath = builder.vaultPath;
    }

    /**
     * @return Builder for CryptoVault.
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Puts a byte array in the CryptoVault.
     *
     * @param name Value name.
     * @param data Value bytes.
     * @return CryptoVault.
     * @throws IllegalArgumentException when any of the arguments are null, key is empty or blank
     * @throws CryptoVaultException     when method can't manipulate inside of provided directory,
     *                                  or when cipher doesn't like provided key.
     */
    public CryptoVault put(String name, byte[] data) throws CryptoVaultException {
        if (name == null || name.isEmpty() || name.isBlank()) throw new IllegalArgumentException("Invalid name");
        if (data == null) throw new IllegalArgumentException("Value is null");

        Path encPath = getEncPath(this.vaultPath, name);

        FileOutputStream fos;
        try {
            fos = pathToFileOutputStream(encPath);
        } catch (IOException e) {
            String exMsg = String.format("%s: %s\n", encPath, e);
            throw new CryptoVaultException(exMsg);
        }

        InputStream is = new ByteArrayInputStream(data);
        try {
            this.cipher.init(Cipher.ENCRYPT_MODE, this.secretKey);
        } catch (InvalidKeyException e) {
            throw new CryptoVaultException(e);
        }

        CipherInputStream cis = new CipherInputStream(is, this.cipher);
        try {
            int read;
            byte[] buf = new byte[1024];
            while ((read = cis.read(buf)) != -1) {
                fos.write(buf, 0, read);
            }
        } catch (IOException e) {
            String exMsg = String.format("Can't save bytes to file: %s\n", e);
            throw new CryptoVaultException(exMsg);
        }
        return this;

    }

    /**
     * Puts a value in the CryptoVault.
     *
     * @param name  Name for value.
     * @param val   Value.
     * @param codec Codec for value type.
     * @param <T>   Value type.
     * @return CryptoVault.
     * @throws IllegalArgumentException when any of the arguments are null, key is empty or blank
     * @throws CryptoVaultException     when method can't manipulate inside of provided directory,
     *                                  or when cipher doesn't like provided key.
     * @throws CodecException           when codec can't encode value to bytes,
     *                                  or when for given value PutGet property is not satisfied.
     */
    public <T> CryptoVault put(String name, T val, Codec<T> codec) throws CryptoVaultException, CodecException {

        if (name == null || name.isEmpty() || name.isBlank()) throw new IllegalArgumentException("Invalid name");
        if (val == null) throw new IllegalArgumentException("Value is null");
        if (codec == null) throw new IllegalArgumentException("Codec is null");
//        FIXME: There should be some check that we can retrieve value,
//               but to check that on every put will be expensive
//        if (!codec.getPutProperty(val))
//            throw new CodecException(String.format("Broken GetPut property for value %s", val));

        InputStream is = codec.from(val);

        Path encPath = getEncPath(this.vaultPath, name);
        FileOutputStream fos;
        try {
            fos = pathToFileOutputStream(encPath);
        } catch (IOException e) {
            String exMsg = String.format("%s: %s\n", encPath, e);
            throw new CryptoVaultException(exMsg);
        }

        try {
            this.cipher.init(Cipher.ENCRYPT_MODE, this.secretKey);
        } catch (InvalidKeyException e) {
            throw new CryptoVaultException(e);
        }

        CipherInputStream cis = new CipherInputStream(is, this.cipher);

        try {
            int read;
            byte[] buf = new byte[1024];
            while ((read = cis.read(buf)) != -1) {
                fos.write(buf, 0, read);
            }
        } catch (IOException e) {
            String exMsg = String.format("Can't save bytes to file: %s\n", e);
            throw new CryptoVaultException(exMsg);
        }
        return this;

    }

    /**
     * Gets bytes from a CryptoVault.
     *
     * @param name Value name.
     * @return Bytes, or null, if name doesn't map to a stored value.
     * @throws IllegalArgumentException when key is null, or empty, or blank
     * @throws CryptoVaultException     when method can't manipulate inside of provided directory,
     *                                  or when cipher doesn't like provided key.
     */
    public byte[] get(String name) throws CryptoVaultException {
        if (name == null || name.isEmpty() || name.isBlank()) throw new IllegalArgumentException("Invalid name");

        Path encPath = getEncPath(this.vaultPath, name);
        FileInputStream fis;
        try {
            fis = pathToFileInputStream(encPath);
        } catch (IOException e) {
            return null;
        }

        try {
            this.cipher.init(Cipher.DECRYPT_MODE, this.secretKey);
        } catch (InvalidKeyException e) {
            throw new CryptoVaultException(e);
        }

        CipherInputStream cis = new CipherInputStream(fis, this.cipher);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            int read;
            byte[] buf = new byte[1024];
            while ((read = cis.read(buf)) != -1) {
                baos.write(buf, 0, read);
            }
            return baos.toByteArray();
        } catch (IOException e) {
            throw new CryptoVaultException(e);
        }

    }

    /**
     * Gets value from a CryptoVault.
     *
     * @param name  Value name.
     * @param codec Codec for value type.
     * @param <T>   Value type.
     * @return Value, or null, if name doesn't map to a stored value.
     * @throws IllegalArgumentException when any of the arguments are null, key is empty or blank
     * @throws CryptoVaultException     when method can't manipulate inside of provided directory,
     *                                  or when cipher doesn't like provided key.
     * @throws CodecException           when codec can't encode value to bytes,
     *                                  or when for given value PutGet property is not satisfied.
     */
    public <T> T get(String name, Codec<T> codec) throws CryptoVaultException, CodecException {
        if (name == null || name.isEmpty() || name.isBlank()) throw new IllegalArgumentException("Invalid name");
        if (codec == null) throw new IllegalArgumentException("Codec is null");


        Path encPath = getEncPath(this.vaultPath, name);
        FileInputStream fis;

        try {
            fis = pathToFileInputStream(encPath);
        } catch (IOException e) {
            return null;
        }

        try {
            this.cipher.init(Cipher.DECRYPT_MODE, this.secretKey);
        } catch (InvalidKeyException e) {
            throw new CryptoVaultException(e);
        }

        CipherInputStream cis = new CipherInputStream(fis, this.cipher);
        return codec.to(cis);
    }

    /**
     * Removes stored value.
     * If no value is mapped to given key, this method is a no-op.
     *
     * @param name Value name.
     * @return CryptoVault.
     */
    public CryptoVault remove(String name) {
        Path encPath = getEncPath(this.vaultPath, name);
        encPath.toFile().delete();
        return this;
    }

    /**
     * Clears a CryptoVault.
     * Note that this method doesn't remove a vault directory.
     *
     * @return CryptoVault.
     * @throws CryptoVaultException when method can't access files for deletion.
     */
    public CryptoVault clear() throws CryptoVaultException {
        try {
            Files.walk(this.vaultPath, FileVisitOption.FOLLOW_LINKS)
                    .forEach(f -> f.toFile().delete());
        } catch (IOException e) {
            throw new CryptoVaultException(e);
        }
        return this;
    }

    private Path getEncPath(Path path, String name) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new CryptoVaultException.Unchecked(e);
        }
        String encName = Base64.getEncoder().encodeToString(digest.digest(name.getBytes()))
                .replace(File.separatorChar, '_')
                .replace('=', '-');

        return Paths.get(path.toAbsolutePath().toString(), encName);
    }

    private FileOutputStream pathToFileOutputStream(Path path) throws IOException {
        File f = path.toFile();
        if (!f.exists()) {
            if (!f.createNewFile()) {
                String exMsg = String.format("Can't create file at %s.", path);
                throw new IOException(exMsg);
            }
        }
        return new FileOutputStream(f);
    }

    private FileInputStream pathToFileInputStream(Path path) throws IOException {
        return new FileInputStream(path.toFile());
    }

    /**
     * To be able to store and get value of given type, codec should exist for that type.
     * Note that codec must satisfy following property:
     * <p> ∀x:T . to(from(x)) == x </p>
     *
     * @param <T> Type of serializable value.
     */
    public abstract static class Codec<T> {
        public abstract InputStream from(T val) throws CodecException;

        public abstract T to(InputStream bais) throws CodecException;

        boolean getPutProperty(T val) {
            try {
                InputStream is = this.from(val);
                T newVal = this.to(is);
                return val.equals(newVal);
            } catch (CodecException e) {
                System.err.println(e.getMessage());
                return false;
            }
        }
    }

    /**
     * Builder for CryptoVault.
     */
    public static class Builder {
        private SecretKey secretKey;
        private Cipher cipher;
        private Path vaultPath;

        private Builder() {
        }

        /**
         * Sets a symmetric key.
         *
         * @param secretKey Key.
         * @return Builder.
         */
        public Builder secretKey(SecretKey secretKey) {
            this.secretKey = secretKey;
            return this;
        }

        /**
         * Sets a cipher with a given transformation.
         *
         * @param transformation Transformation for a cipher.
         * @return Builder.
         * @throws IllegalArgumentException when transformation is invalid.
         */
        public Builder cipher(String transformation) {
            Cipher cipher;
            try {
                cipher = Cipher.getInstance(transformation);
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                throw new IllegalArgumentException(e);
            }
            this.cipher = cipher;
            return this;
        }

        /**
         * Sets a path to a vault directory.
         * Note that directory must be readable and writable.
         *
         * @param vaultPath Path to a vault.
         * @return Builder.
         * @throws IllegalArgumentException when supplied path is not a directory, or not readable/writable.
         */
        public Builder vault(Path vaultPath) {
            if (!vaultPath.toFile().isDirectory()) {
                String exMsg = String.format("%s must be a directory", vaultPath.toAbsolutePath().toString());
                throw new IllegalArgumentException(exMsg);
            }
            if (!vaultPath.toFile().canRead()) {
                String exMsg = String.format("%s must be readable", vaultPath.toAbsolutePath().toString());
                throw new IllegalArgumentException(exMsg);
            }
            if (!vaultPath.toFile().canWrite()) {
                String exMsg = String.format("%s must be writable", vaultPath.toAbsolutePath().toString());
                throw new IllegalArgumentException(exMsg);
            }
            this.vaultPath = vaultPath;
            return this;
        }

        /**
         * "Opens" a CryptoVault.
         *
         * @return CryptoVault.
         */
        public CryptoVault open() {
            return new CryptoVault(this);
        }
    }
}
