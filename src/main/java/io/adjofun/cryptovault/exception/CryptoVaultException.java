package io.adjofun.cryptovault.exception;

/**
 * General exception used in this library.
 */
public class CryptoVaultException extends Exception {
    public CryptoVaultException(String err) {
        super(err);
    }

    public CryptoVaultException(Throwable t) {
        super(t);
    }

    /**
     * Unchecked version of general exception.
     */
    public static class Unchecked extends RuntimeException {
        public Unchecked(String err) {
            super(err);
        }

        public Unchecked(Throwable t) {
            super(t);
        }
    }
}
