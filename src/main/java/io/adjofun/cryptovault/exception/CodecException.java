package io.adjofun.cryptovault.exception;

/**
 * Exceptions thrown by Codecs.
 */
public class CodecException extends Exception {
    public CodecException(String err) {
        super(err);
    }

    public CodecException(Throwable t) {
        super(t);
    }
}
