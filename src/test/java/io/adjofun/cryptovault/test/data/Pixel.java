package io.adjofun.cryptovault.test.data;

import io.adjofun.cryptovault.CryptoVault;
import io.adjofun.cryptovault.exception.CodecException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * This is a example class with a codec, used in tests.
 */
public class Pixel {
    public static final Codec CODEC = new Codec();
    private byte r;
    private byte g;
    private byte b;

    public Pixel(byte r, byte g, byte b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Pixel)) return false;
        Pixel pixel = (Pixel) obj;
        return this.r == pixel.r && this.g == pixel.g && this.b == pixel.b;
    }

    private static class Codec extends CryptoVault.Codec<Pixel> {
        @Override
        public InputStream from(Pixel val) throws CodecException {
            return new ByteArrayInputStream(new byte[]{val.r, val.g, val.b});
        }

        @Override
        public Pixel to(InputStream bais) throws CodecException {
            int read;
            byte[] rgb = new byte[3];
            try {
                read = bais.read(rgb);
            } catch (IOException e) {
                throw new CodecException(e);
            }
            if (read != 3) {
                String exMsg = String.format("Not enough bytes in InputStream: expected 3, got %d", read);
                throw new CodecException(exMsg);
            }
            return new Pixel(rgb[0], rgb[1], rgb[2]);
        }
    }
}
