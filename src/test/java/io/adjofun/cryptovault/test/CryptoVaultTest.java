package io.adjofun.cryptovault.test;

import io.adjofun.cryptovault.CryptoVault;
import io.adjofun.cryptovault.test.data.Pixel;
import org.junit.Assert;
import org.junit.Test;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.util.Random;


public class CryptoVaultTest {
    private SecretKey getSecretKey() throws Exception {
        KeyStore keyStore = KeyStore.getInstance("jks");
        InputStream ks = CryptoVaultTest.class.getClassLoader().getResourceAsStream("testkeystore.jceks");
        keyStore.load(ks, "changeit".toCharArray());
        return (SecretKey) keyStore.getKey("test", "changeit".toCharArray());
    }

    private Path getVaultPath() {
        try {
            return Files.createTempDirectory("testcryptovault");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getsWhatItPuts() throws Exception {

        CryptoVault cv = CryptoVault.builder()
                .secretKey(getSecretKey())
                .cipher("AES")
                .vault(getVaultPath())
                .open();

        Pixel pixel = new Pixel((byte) 255, (byte) 255, (byte) 255);
        Pixel pixelFromCV = cv.put("foo", pixel, Pixel.CODEC)
                .get("foo", Pixel.CODEC);

        Assert.assertEquals(pixel, pixelFromCV);

    }

    @Test
    public void putGetForByteArrays() throws Exception {
        CryptoVault cv = CryptoVault.builder()
                .secretKey(getSecretKey())
                .cipher("AES")
                .vault(getVaultPath())
                .open();

        byte[] data = new byte[10];
        new Random().nextBytes(data);

        byte[] fromCV = cv.put("quux", data).get("quux");

        Assert.assertArrayEquals(data, fromCV);
    }

    @Test
    public void putIsIdempotent() throws Exception {
        CryptoVault cv = CryptoVault.builder()
                .secretKey(getSecretKey())
                .cipher("AES")
                .vault(getVaultPath())
                .open();

        Pixel pixel = new Pixel((byte) 255, (byte) 255, (byte) 255);
        Pixel pixelFromCV = cv.put("bar", pixel, Pixel.CODEC)
                .put("bar", pixel, Pixel.CODEC)
                .get("bar", Pixel.CODEC);

        Assert.assertEquals(pixel, pixelFromCV);
    }

    @Test
    public void removesStoredValue() throws Exception {
        CryptoVault cv = CryptoVault.builder()
                .secretKey(getSecretKey())
                .cipher("AES")
                .vault(getVaultPath())
                .open();

        Pixel pixel = new Pixel((byte) 255, (byte) 255, (byte) 255);
        Pixel pixelFromCV = cv.put("baz", pixel, Pixel.CODEC)
                .remove("baz")
                .get("baz", Pixel.CODEC);
        Assert.assertNull(pixelFromCV);
    }

    @Test
    public void clearsVault() throws Exception {
        CryptoVault cv = CryptoVault.builder()
                .secretKey(getSecretKey())
                .cipher("AES")
                .vault(getVaultPath())
                .open();

        Pixel pixel = new Pixel((byte) 255, (byte) 255, (byte) 255);
        cv.put("fizz", pixel, Pixel.CODEC)
                .put("buzz", pixel, Pixel.CODEC)
                .put("fizzbuzz", pixel, Pixel.CODEC)
                .clear();
        Assert.assertNull(cv.get("fizz", Pixel.CODEC));
        Assert.assertNull(cv.get("buzz", Pixel.CODEC));
        Assert.assertNull(cv.get("fizzbuzz", Pixel.CODEC));
    }
}
